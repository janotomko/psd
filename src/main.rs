use chrono::{
    DateTime,
    Utc
};
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use figlet_rs::FIGfont;
use serde::{Deserialize, Serialize};
use std::{
    io,
    time::{Duration, Instant},
};
use tui::{
    backend::{Backend, CrosstermBackend},
    layout::{Alignment, Constraint, Layout},
    style::{Color, Style},
    widgets::{Block, Paragraph},
    Frame, Terminal,
};

#[derive(Debug, Serialize, Deserialize)]
struct PsdConfig {
    font: String,
    tick: u64,
}

impl Default for PsdConfig {
    fn default() -> Self{
        PsdConfig {
            font: "/usr/share/figlet/standard.flf".to_string(),
            tick: 250,
        }
    }
}

struct App {
    cfg: PsdConfig,
    font: FIGfont,
}

impl App {
    fn new(cfg: PsdConfig) -> App {
        let font = FIGfont::from_file(&cfg.font).unwrap();
        App {
            cfg,
            font,
        }
    }
}

fn main() -> Result<(), io::Error> {
    let cfg = confy::load("psd").unwrap();
    let app = App::new(cfg);

    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let res = run_app(&mut terminal, app);

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{:?}", err)
    }

    Ok(())
}

fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App,
) -> io::Result<()> {
    let mut last_tick = Instant::now();
    let tick_rate = Duration::from_millis(app.cfg.tick);
    loop {
        terminal.draw(|f| ui(f, &app))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));
        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                if let KeyCode::Char('q') = key.code {
                    return Ok(());
                }
            }
        }
        if last_tick.elapsed() >= tick_rate {
            last_tick = Instant::now();
        }
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &App) {
    let size = f.size();

    let chunks = Layout::default()
        .constraints(
            [
                Constraint::Min(5),
                Constraint::Min(0)
            ]
            .as_ref(),
        )
        .split(size);

    let now: DateTime<Utc> = Utc::now();
    let clock =
        if now.timestamp() % 2 == 0 {
            now.format("%H : %M : %S")
        } else {
            now.format("%H    %M    %S")
        }.to_string();
    let figure = app.font.convert(&clock).unwrap();
    let paragraph = Paragraph::new(figure.to_string())
        .style(Style::default()
            .fg(Color::Cyan)
            .bg(Color::Black))
        .block(Block::default())
        .alignment(Alignment::Center);
    f.render_widget(paragraph, chunks[0]);
}
